Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bat
Upstream-Contact: David Peter <mail@david-peter.de>
Source: https://github.com/sharkdp/bat

Files: *
Copyright: 
 2018-2021 David Peter <mail@david-peter.de>
 2018-2022 bat-developers (https://github.com/sharkdp/bat).
License: MIT or Apache-2.0

Files: tests/benchmarks/highlighting-speed-src/miniz.c
Copyright:
 2013-2014 RAD Game Tools and Valve Software
 2010-2014 Rich Geldreich and Tenacious Software LLC
License: MIT

Files: tests/syntax-tests/source/Bash/batgrep.sh
Copyright:
 2019-2020 eth-p and contributors
License: MIT

Files: tests/syntax-tests/source/Cabal/semantic.cabal
Copyright: 2015-2019 GitHub
License: MIT

Files: tests/syntax-tests/source/CoffeeScript/coffeescript.coffee
Copyright: 2009-2018 Jeremy Ashkenas
License: MIT

Files: tests/syntax-tests/source/DotENV/.env
Copyright: 2016 Zayn Ali
License: MIT

Files: tests/syntax-tests/source/Elixir/command.ex
Copyright: 2020 José Eduardo
License: MIT

Files: tests/syntax-tests/source/Elm/test.elm
Copyright: 2012-present Evan Czaplicki
License: 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer. 
 .
 * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.
 .
 * Neither the name of Evan Czaplicki nor the names of other
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: tests/syntax-tests/source/Fortran (Fixed Form)/quicksort_real_F77.F
Copyright: 2019 Jason Allen Anema
License: MIT

Files: tests/syntax-tests/source/Fortran (Modern)/test_savetxt.f90
Copyright: 2019 Fortran stdlib developers
License: MIT

Files: 
 tests/syntax-tests/source/Git Ignore/.gitignore
 tests/syntax-tests/source/Groff/*
Copyright:
 2006-2009 Graydon Hoare
 2009-2012 Mozilla Foundation
 2012-2020 The Rust Project Developers
License: MIT or Apache-2.0

Files: tests/syntax-tests/source/Graphviz DOT/*
Copyright: 2014 Pavel Roschin
License: MIT

Files: tests/syntax-tests/source/Java Server Page (JSP)/sessionDetail.jsp
Copyright: 2005-2021 the Apache Software Foundation
License: Apache-2.0

Files: tests/syntax-tests/source/Lisp/utils.lisp
Copyright: 2009-2010 Kat Marchán
License: MIT

Files: tests/syntax-tests/source/LiveScript/livescript-demo.ls
Copyright: 2012 Paul Miller (http://paulmillr.com/), Jeremy Ashkenas
License: MIT

Files: tests/syntax-tests/source/MATLAB/test.matlab
Copyright: 
 2020 Bryan W. Weber
 2020 Georg Brandl
License: BSD-2-clause

Files: tests/syntax-tests/source/Makefile/Makefile
Copyright: 2006-2020, Salvatore Sanfilippo
License: 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the distribution.
 * Neither the name of Redis nor the names of its contributors may be used
   to endorse or promote products derived from this software without 
   specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: tests/syntax-tests/source/NAnt Build File/Default.build
Copyright: 2007-2009 Travis Illig
License: MIT

Files: tests/syntax-tests/source/Robot Framework/recipe141_aws_simple_storage_service.robot
Copyright: 2020 Adrian Yorke
License: MIT

Files: tests/syntax-tests/source/Solidity/ERC721.sol
Copyright: 2016-2020 zOS Global Limited
License: MIT

Files: tests/syntax-tests/source/Stylus/gradients.styl
Copyright: 2014 TJ Holowaychuk <tj@vision-media.ca>
License: MIT

Files: tests/syntax-tests/source/Svelte/App.svelte
Copyright: 
 2019 pngwn <hello@pngwn.io>
 2021 BabakFP <44144724+babakfp@users.noreply.github.com>
License: MIT

Files: tests/syntax-tests/source/TypeScriptReact/app.tsx
Copyright: 2016 brauliodiez
License: MIT

Files: tests/syntax-tests/source/Verilog/div_pipelined.v
Copyright: 2018 Schuyler Eldridge
License: Apache-2.0

Files: tests/syntax-tests/source/dash/shfm
Copyright: 2020 Dylan Araps
License: MIT

Files: tests/syntax-tests/source/varlink/org.varlink.certification.varlink
Copyright: 2016 - 2018 Red Hat, Inc.
License: MIT or Apache-2.0

Files: debian/*
Copyright:
 2018-2021 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2018-2019 Paride Legovini <paride@debian.org>
      2019 Helen Koike <helen@koikeco.de>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.