Source: rust-coreutils
Section: utils
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 24),
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-backtrace-dev,
 librust-half-1.6+default-dev | librust-half-1.3+default-dev | librust-half-1.2+default-dev | librust-half-1.1+default-dev,
 librust-lazy-static-1+default-dev (>= 1.3-~~),
 librust-same-file-1.0.6+default-dev | librust-same-file-1.0.5+default-dev | librust-same-file-1.0.4+default-dev,
 librust-textwrap-0.11.0+default-dev,
 librust-textwrap-0.11.0+term-size-dev, librust-cc-dev,
 librust-quick-error-dev, librust-unix-socket-dev, librust-getopts-dev, librust-filetime-dev,
 librust-walkdir-dev, librust-ioctl-sys-dev, librust-xattr-dev, librust-number-prefix-dev,
 librust-rust-ini-dev, librust-failure-dev, librust-failure-derive-dev,
 librust-glob-dev, librust-chrono-dev, librust-onig-dev,
 librust-blake2-rfc-dev, librust-digest-dev, librust-hex-dev, librust-md5-dev,
 librust-sha1-dev, librust-sha2-dev, librust-sha3-dev,
 librust-atty-dev, librust-term-grid-dev, librust-termsize-dev,
 librust-tempfile-dev, librust-nix-dev, librust-fs-extra-dev,
 librust-itertools-dev, librust-bit-set-dev, librust-dunce-dev,
 librust-data-encoding-dev, librust-platform-info-dev (>= 0.1),
 librust-num-traits-dev (>= 0.2.14), librust-smallvec-dev,
 librust-rand-pcg-dev, librust-thiserror-dev, librust-lazy-static-dev,
 librust-byteorder-dev, librust-hostname-dev (>= 0.3.1), librust-cpp-build-dev,
 librust-fnv-dev, librust-wild-dev, librust-cpp-dev, librust-thread-local-dev,
 librust-backtrace-sys-dev, python3-sphinx, help2man
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/coreutils]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/coreutils
Homepage: https://github.com/uutils/coreutils
Rules-Requires-Root: no

Package: rust-coreutils
Architecture: any
Multi-Arch: allowed
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: Universal coreutils utils, written in Rust
 This packages replaces the GNU coreutils package written in C.
 It should be a drop-in replacement but:
  * Some options have NOT been implemented,
  * A few binaries are missing (ex: dd),
  * Might have important bugs,
  * Might be slower,
  * Output of the binaries might be slightly different.
 .
 It might break your system.
 .
 This is an EXPERIMENTAL package. It should not be used in
 production.
