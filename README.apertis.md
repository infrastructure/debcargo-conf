# Apertis-specific debcargo usage

## Building debcargo

Thus far, all testing has been with [debcargo
2.4.4](https://salsa.debian.org/rust-team/debcargo/-/tree/2.4.4), but with the
`Cargo.toml` patched to set the version of `serde` to `=1.0.121`.

## Working with an unreleased package

- `./repackage.sh $CRATE_NAME` will process all of the packaging-related files
  under `src/$CRATE_NAME/` and generate a Debian package source tree under
  `build/$CRATE_NAME/`.
  - If you re-run this after some changes to the package files, but it does not
    output anything, it didn't detect any changes since the last run. Delete
    `build/CRATE_NAME/debian/changelog`, then run again.
- If you want to update the patches, `cd build/$CRATE_NAME/`, `rm -rf .pc`, then
  [use quilt as usual](https://wiki.debian.org/UsingQuilt). Once you're done,
  copy the new contents of `debian/patches` to `src/$CRATE_NAME/debian/patches`,
  and re-package it as mentioned above.
- `cd build` and
- `DISTRIBUTION=$APERTIS_RELEASE SOURCEONLY=1 ./build.sh $CRATE_NAME`
  will build a `.dsc` file for the crate. Doing a full build here does not work,
  as it depends on `sbuild`, which is not packaged in Apertis.
  - Similarly to `.repackage.sh`, if you re-run `./build.sh` after some changes
    but it does not output anything, `rm $CRATE_NAME*.dsc` and try again.
  - If it depends on some other crates that aren't packaged in Apertis yet, make
    sure they're already packaged and installed, and then
    running `apt-cache show $OTHER_CRATE >> build/dpkg-dummy/status` should make
    `./build.sh` pick up on the dependency.
- Once that has completed,
  [import-debcargo-dsc](https://gitlab.apertis.org/refi64/import-debcargo-dsc)
  can be used to generate a package repository, akin to `import-debian-package`.
  - `import-debcargo-dsc.sh $APERTIS_RELEASE $DSC_FILE $COMPONENT` will create
    a new directory named after the package with a Git repo inside
  - Re-run in the same directory on an updated .dsc file to import your changes.
